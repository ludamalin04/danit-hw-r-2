
const HeaderHero = ({children}) => {
    return (
        <div className='header-hero'>
            <h1 className='header-name'>Blue Band</h1>
            <h2 className='header-title'>краса в кожному моменті</h2>
            {children}
        </div>
    );
};

export default HeaderHero;