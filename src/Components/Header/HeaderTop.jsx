import PropTypes from "prop-types";
// import Heart from "../Controllers/Heart.jsx";

const HeaderTop = ({children}) => {
    return (
        <div className='header-top'>
            <div>
                <a className='logo' href='#'><img src='../../../public/imgs/Logo-Blueband.png' alt='logo'/></a>
            </div>
            <ul className='header-top-menu'>
                <li><a className='header-top-item' href='#'>Насіння</a></li>
                <li><a className='header-top-item' href='#'>Саджанці</a></li>
                <li><a className='header-top-item' href='#'>Однорічні</a></li>
                <li><a className='header-top-item' href='#'>Багаторічні</a></li>
                <li><a className='header-top-item' href='#'>Вічнозелені</a></li>
            </ul>
            <div className='header-top-action'>
                {children}
            </div>
        </div>
    );
};

HeaderTop.propTypes = {
    children: PropTypes.any,
    // handleFavorite: PropTypes.func
}
export default HeaderTop;