import PropTypes from "prop-types";

const ModalHeader = ({children}) => {
    return (
        <div className='modal-header'>
            {children}
        </div>
    );
};

export default ModalHeader;

ModalHeader.propTypes = {
    children: PropTypes.any
}