import PropTypes from "prop-types";

const ModalClose = ({click}) => {
    return (
        <div onClick={click}>
            <img src="../../../public/imgs/Icon.png" alt='close-icon'/>
        </div>
    );
};

export default ModalClose;

ModalClose.propTypes = {
    click: PropTypes.func
}