import PropTypes from "prop-types";
import "./Controllers.scss"

const Controllers = ({children}) => {
    return (
        <div className="btn-container">
            {children}
        </div>
    );
};

export default Controllers;

Controllers.propTypes = {
    children: PropTypes.any
}