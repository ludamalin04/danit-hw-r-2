import PropTypes from "prop-types";
import Heart from "../Controllers/Heart.jsx";
import Button from "../Controllers/Button.jsx";

const ShopCard = ({
                      color,
                      article,
                      price,
                      name,
                      img,
                      alt,
                      handleCurrentCard,
                      actionFirst,
                      handleFavorite,
                      favHeart,
                      buyBtn,
                      colorClick
                  }) => {
    const item = {color, article, price, name, img, alt}

    return (
        <div className='shop-card'>
            <div>
                <img src={img} alt={alt}/>
            </div>
            <div className='shop-card-favorite'>
                <h3 className='shop-card-name'>{name}</h3>
                <Heart
                    article={article}
                    classNames={favHeart.includes(article) ? "favheart" : ""}
                    click={() => {
                        colorClick(article)
                        handleFavorite(item)
                    }}/>
            </div>
            <div className='shop-card-content'>
                <span className='shop-card-article'>артикул {article}</span>
                <h3 className='shop-card-color'>{color}</h3>
                <div className='shop-card-buy'>
                    <h3><span className='shop-card-price'>{price}</span>грн</h3>
                    <Button
                            classNames={buyBtn.includes(article) ? "buy-btn-active" : "buy-btn"}
                            click={() => {
                        actionFirst()
                        handleCurrentCard(item)
                    }}>КУПИТИ</Button>
                </div>
            </div>
        </div>
    )
};

ShopCard.propTypes = {
    children: PropTypes.any,
    handleCurrentCard: PropTypes.func,
    handleFavorite: PropTypes.func,
    actionFirst: PropTypes.func,
    colorClick: PropTypes.func,
    color: PropTypes.string,
    article: PropTypes.number,
    price: PropTypes.number,
    name: PropTypes.string,
    favHeart: PropTypes.any,
    buyBtn: PropTypes.any,
    item: PropTypes.any,
    img: PropTypes.string,
    alt: PropTypes.string,
}
export default ShopCard;