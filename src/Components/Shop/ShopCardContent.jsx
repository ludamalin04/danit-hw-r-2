// // import Heart from "../Controllers/Heart.jsx";
// import PropTypes from "prop-types";
//
// const ShopCardContent = ({color, article, price, children}) => {
//     return (
//         <div className='shop-card-content'>
//             <span className='shop-card-article'>артикул {article}</span>
//             <h3 className='shop-card-color'>{color}</h3>
//             <div className='shop-card-buy'>
//                 <h3><span className='shop-card-price'>{price}</span>грн</h3>
//                 {children}
//             </div>
//         </div>
//     );
// };
//
// ShopCardContent.propTypes = {
//     color: PropTypes.string,
//     article: PropTypes.number,
//     price: PropTypes.number,
//     children: PropTypes.any,
//     // children1: PropTypes.any
// }
// export default ShopCardContent;