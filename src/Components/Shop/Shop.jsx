import "./Shop.scss";
import PropTypes from "prop-types";

const Shop = ({children}) => {
    return (
        <div className='shop'>{children}</div>
    );
};

Shop.propTypes = {
    children: PropTypes.any,
}
export default Shop;