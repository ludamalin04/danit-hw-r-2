import {useEffect, useState} from "react";
import {sendRequest} from "./Helpers/SendRequest.jsx";
import Button from "./Components/Controllers/Button.jsx";
import ModalImage from "./Components/Modal/ModalBasic.jsx";
import ModalWrapper from "./Components/Modal/ModalWrapper.jsx";
import Modal from "./Components/Modal/Modal.jsx";
import ModalHeader from "./Components/Modal/ModalHeader.jsx";
import ModalClose from "./Components/Modal/ModalClose.jsx";
import ModalBody from "./Components/Modal/ModalBody.jsx";
import ModalFooter from "./Components/Modal/ModalFooter.jsx";
import Shop from "./Components/Shop/Shop.jsx";
import HeaderTop from "./Components/Header/HeaderTop.jsx";
import Header from "./Components/Header/Header.jsx";
import HeaderHero from "./Components/Header/HeaderHero.jsx";
import ShopCard from "./Components/Shop/ShopCard.jsx";
import Heart from "./Components/Controllers/Heart.jsx";
import Cart from "./Components/Controllers/Cart.jsx";

const favoriteFromLocalStorage = JSON.parse(localStorage.getItem('favorite')) || [];
const favHeartFromLocalStorage = JSON.parse(localStorage.getItem('favHeart')) || [];
const shopFromLocalStorage = JSON.parse(localStorage.getItem('shop')) || [];
const buyBtnFromLocalStorage = JSON.parse(localStorage.getItem('buyBtn')) || [];

function App() {

// рендер карток з json
    const [flowersArr, setFlowersArr] = useState([]);
    useEffect(() => {
        sendRequest("/data.json")
            .then((flowers) => {
                setFlowersArr(flowers)
            })
    }, []);

// додавання у вибране
    const [favorite, setFavorite] = useState(favoriteFromLocalStorage);
    const handleFavorite = (item) => {
        const isFavorite = favorite.some((fav) => fav.article === item.article)
        if (isFavorite) {
            const existFav = favorite.filter((fav) => fav.article !== item.article);
            setFavorite(existFav);
        } else {
            setFavorite([...favorite, item]);
        }
    };

// зміна кольору серця
    const [favHeart, setFavHeart] = useState(favHeartFromLocalStorage);
    const colorClick = (article) => {
        setFavHeart((prev) => {
            return prev.includes(article) ? prev.filter((number) => number !== article) : [...prev, article]
        })
    };

// збереження вибраного в localstorage
    useEffect(() => {
        localStorage.setItem('favorite', JSON.stringify(favorite));
    }, [favorite]);

// збереження зміни кольору серця в localstorage
    useEffect(() => {
        localStorage.setItem('favHeart', JSON.stringify(favHeart));
    }, [favHeart]);

// додавання у кошик в модалці
    const [shop, setShop] = useState(shopFromLocalStorage);
    const handleShop = (item) => {
        setShop([...shop, item]);
    };

// збереження кошика в localstorage
    useEffect(() => {
        localStorage.setItem('shop', JSON.stringify(shop));
    }, [shop]);

// відображення інфо з картки у модалці
    const [currentCard, setCurrentCard] = useState({});
    const handleCurrentCard = (card) => setCurrentCard(card);

// відкриття і закриття першої модалки
    const [isOpenFirst, setIsOpenFirst] = useState(false);
    const actionFirst = () => {
        setIsOpenFirst(!isOpenFirst)
    };

// зміна кольору кнопки
    const [buyBtn, setBuyBtn] = useState(buyBtnFromLocalStorage);
    const buyClick = (article) => {
        setBuyBtn((prev) => {
            return prev.includes(article)
                ? prev.filter((number) => number !== article)
                : [...prev, article]
        })
    }

// збереження зміни кольору кнопки в localstorage
    useEffect(() => {
        localStorage.setItem('buyBtn', JSON.stringify(buyBtn));
    }, [buyBtn]);

    return (
        <>
            <Header>
                <HeaderTop>
                    <Heart heartColor='none' classNames='heart'/>
                    <span className='favorite-count'>{favorite.length}</span>
                    <Cart/>
                    <span>{shop.length}</span>
                </HeaderTop>
                <HeaderHero>
                    <Button classNames='hero-btn'>до покупок</Button>
                </HeaderHero>
            </Header>
            <Shop>
                {
                    flowersArr.map(({article, name, url, price, color}) => {
                        return (
                            <ShopCard
                                actionFirst={actionFirst}
                                handleFavorite={handleFavorite}
                                handleCurrentCard={handleCurrentCard}
                                colorClick={colorClick}
                                buyBtn={buyBtn}
                                favHeart={favHeart}
                                key={article}
                                alt={name}
                                img={url}
                                name={name}
                                article={article}
                                price={price}
                                color={color}
                            >
                            </ShopCard>
                        )
                    })
                }
            </Shop>
            {isOpenFirst && (
                <ModalImage>
                    <ModalWrapper click={actionFirst}>
                        <Modal>
                            <ModalHeader>
                                <ModalClose click={actionFirst}/>
                            </ModalHeader>
                            <ModalBody
                                url={currentCard.img}
                                title={currentCard.name}
                                desc={currentCard.price}>
                            </ModalBody>
                            <ModalFooter>
                                <Button click={actionFirst} classNames="modal-btn">СКАСУВАТИ</Button>
                                <Button click={() => {
                                    actionFirst()
                                    handleShop(currentCard)
                                    buyClick(currentCard.article)
                                }}
                                        classNames="modal-btn-active"
                                        article={currentCard.article}>ДОДАТИ В КОШИК</Button>
                            </ModalFooter>
                        </Modal>
                    </ModalWrapper>
                </ModalImage>)}
        </>
    )
}

export default App
